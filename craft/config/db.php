<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
	'*' => array(
      'tablePrefix' => 'craft',
  ),

  'dev' => array(
      'server' => 'localhost',
      'user' => 'e39b766ddf0a',
      'password' => '77eae3533ead3fff',
      'database' => 'water-institute',
  ),

  // 'production' => array(
  //     'server' => 'localhost',
  //     'user' => 'av12345',
  //     'password' => '$uP3r$3jp3t',
  //     'database' => 'av12345-buildwithcraft',
  // ),


	'initSQLs' => array("SET SESSION sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")

);
