<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

	'*' => array(
  	'omitScriptNameInUrls' => true,
  	// Default Week Start Day (0 = Sunday, 1 = Monday...)
		'defaultWeekStartDay' => 0,

		// Enable CSRF Protection (recommended, will be enabled by default in Craft 3)
		'enableCsrfProtection' => true,

		// Control Panel trigger word
		'cpTrigger' => 'admin',

		'maxUploadFileSize' => 52428800,
  ),

  'dev' => array(
      'siteUrl' => 'http://dev.thewaterinstitute.org',
      'devMode' => true,
      'environmentVariables' => array(
			    'baseUrl'  => 'https://dev.thewaterinstitute.org/',
			    'basePath' => '/srv/users/serverpilot/apps/water-institute/public/',
			),
  ),

);
