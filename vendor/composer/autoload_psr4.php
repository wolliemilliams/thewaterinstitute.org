<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer/src'),
    'yii\\queue\\sync\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/sync'),
    'yii\\queue\\sqs\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/sqs'),
    'yii\\queue\\redis\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/redis'),
    'yii\\queue\\gearman\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/gearman'),
    'yii\\queue\\file\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/file'),
    'yii\\queue\\db\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/db'),
    'yii\\queue\\beanstalk\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/beanstalk'),
    'yii\\queue\\amqp_interop\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp_interop'),
    'yii\\queue\\amqp\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp'),
    'yii\\queue\\' => array($vendorDir . '/yiisoft/yii2-queue/src'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug/src'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'yii2tech\\ar\\softdelete\\' => array($vendorDir . '/yii2tech/ar-softdelete/src'),
    'modules\\' => array($baseDir . '/modules'),
    'mikehaertl\\shellcommand\\' => array($vendorDir . '/mikehaertl/php-shellcommand/src'),
    'fruitstudios\\linkit\\' => array($vendorDir . '/fruitstudios/linkit/src'),
    'enshrined\\svgSanitize\\' => array($vendorDir . '/enshrined/svg-sanitize/src'),
    'creocoder\\nestedsets\\' => array($vendorDir . '/creocoder/yii2-nested-sets/src'),
    'craftcms\\oauth2\\client\\' => array($vendorDir . '/craftcms/oauth2-craftid/src'),
    'craft\\redactor\\' => array($vendorDir . '/craftcms/redactor/src'),
    'craft\\composer\\' => array($vendorDir . '/craftcms/plugin-installer/src'),
    'craft\\' => array($vendorDir . '/craftcms/cms/src'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'benf\\neo\\' => array($vendorDir . '/spicyweb/craft-neo/src'),
    'aelvan\\imager\\' => array($vendorDir . '/aelvan/imager/src'),
    'Zend\\Stdlib\\' => array($vendorDir . '/zendframework/zend-stdlib/src'),
    'Zend\\Feed\\' => array($vendorDir . '/zendframework/zend-feed/src'),
    'Zend\\Escaper\\' => array($vendorDir . '/zendframework/zend-escaper/src'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'TrueBV\\' => array($vendorDir . '/true/punycode/src'),
    'Tinify\\' => array($vendorDir . '/tinify/tinify/lib/Tinify'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Stringy\\' => array($vendorDir . '/danielstjules/stringy/src'),
    'Seld\\PharUtils\\' => array($vendorDir . '/seld/phar-utils/src'),
    'Seld\\JsonLint\\' => array($vendorDir . '/seld/jsonlint/src/Seld/JsonLint'),
    'Seld\\CliPrompt\\' => array($vendorDir . '/seld/cli-prompt/src'),
    'SSNepenthe\\ColorUtils\\' => array($vendorDir . '/ssnepenthe/color-utils/src'),
    'Ramsey\\Uuid\\' => array($vendorDir . '/ramsey/uuid/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'LitEmoji\\' => array($vendorDir . '/elvanto/litemoji/src'),
    'League\\OAuth2\\Client\\' => array($vendorDir . '/league/oauth2-client/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'JsonSchema\\' => array($vendorDir . '/justinrainbow/json-schema/src/JsonSchema'),
    'JmesPath\\' => array($vendorDir . '/mtdowling/jmespath.php/src'),
    'ImageOptim\\' => array($vendorDir . '/imageoptim/imageoptim/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Grpc\\Gcp\\' => array($vendorDir . '/google/grpc-gcp/src'),
    'Grpc\\' => array($vendorDir . '/grpc/grpc/src/lib'),
    'Google\\Protobuf\\' => array($vendorDir . '/google/protobuf/src/Google/Protobuf'),
    'Google\\Cloud\\WebSecurityScanner\\' => array($vendorDir . '/google/cloud/WebSecurityScanner/src'),
    'Google\\Cloud\\WebRisk\\' => array($vendorDir . '/google/cloud/WebRisk/src'),
    'Google\\Cloud\\Vision\\' => array($vendorDir . '/google/cloud/Vision/src'),
    'Google\\Cloud\\VideoIntelligence\\' => array($vendorDir . '/google/cloud/VideoIntelligence/src'),
    'Google\\Cloud\\Translate\\' => array($vendorDir . '/google/cloud/Translate/src'),
    'Google\\Cloud\\Trace\\' => array($vendorDir . '/google/cloud/Trace/src'),
    'Google\\Cloud\\TextToSpeech\\' => array($vendorDir . '/google/cloud/TextToSpeech/src'),
    'Google\\Cloud\\Tasks\\' => array($vendorDir . '/google/cloud/Tasks/src'),
    'Google\\Cloud\\Talent\\' => array($vendorDir . '/google/cloud/Talent/src'),
    'Google\\Cloud\\Storage\\' => array($vendorDir . '/google/cloud/Storage/src'),
    'Google\\Cloud\\Speech\\' => array($vendorDir . '/google/cloud/Speech/src'),
    'Google\\Cloud\\Spanner\\' => array($vendorDir . '/google/cloud/Spanner/src'),
    'Google\\Cloud\\SecurityCenter\\' => array($vendorDir . '/google/cloud/SecurityCenter/src'),
    'Google\\Cloud\\Scheduler\\' => array($vendorDir . '/google/cloud/Scheduler/src'),
    'Google\\Cloud\\Redis\\' => array($vendorDir . '/google/cloud/Redis/src'),
    'Google\\Cloud\\PubSub\\' => array($vendorDir . '/google/cloud/PubSub/src'),
    'Google\\Cloud\\OsLogin\\' => array($vendorDir . '/google/cloud/OsLogin/src'),
    'Google\\Cloud\\Monitoring\\' => array($vendorDir . '/google/cloud/Monitoring/src'),
    'Google\\Cloud\\Logging\\' => array($vendorDir . '/google/cloud/Logging/src'),
    'Google\\Cloud\\Language\\' => array($vendorDir . '/google/cloud/Language/src'),
    'Google\\Cloud\\Kms\\' => array($vendorDir . '/google/cloud/Kms/src'),
    'Google\\Cloud\\Iot\\' => array($vendorDir . '/google/cloud/Iot/src'),
    'Google\\Cloud\\Firestore\\' => array($vendorDir . '/google/cloud/Firestore/src'),
    'Google\\Cloud\\ErrorReporting\\' => array($vendorDir . '/google/cloud/ErrorReporting/src'),
    'Google\\Cloud\\Dlp\\' => array($vendorDir . '/google/cloud/Dlp/src'),
    'Google\\Cloud\\Dialogflow\\' => array($vendorDir . '/google/cloud/Dialogflow/src'),
    'Google\\Cloud\\Debugger\\' => array($vendorDir . '/google/cloud/Debugger/src'),
    'Google\\Cloud\\Datastore\\' => array($vendorDir . '/google/cloud/Datastore/src'),
    'Google\\Cloud\\Dataproc\\' => array($vendorDir . '/google/cloud/Dataproc/src'),
    'Google\\Cloud\\Core\\' => array($vendorDir . '/google/cloud/Core/src'),
    'Google\\Cloud\\Container\\' => array($vendorDir . '/google/cloud/Container/src'),
    'Google\\Cloud\\Bigtable\\' => array($vendorDir . '/google/cloud/Bigtable/src'),
    'Google\\Cloud\\BigQuery\\DataTransfer\\' => array($vendorDir . '/google/cloud/BigQueryDataTransfer/src'),
    'Google\\Cloud\\BigQuery\\' => array($vendorDir . '/google/cloud/BigQuery/src'),
    'Google\\Cloud\\AutoMl\\' => array($vendorDir . '/google/cloud/AutoMl/src'),
    'Google\\Cloud\\Asset\\' => array($vendorDir . '/google/cloud/Asset/src'),
    'Google\\Cloud\\' => array($vendorDir . '/google/cloud/src', $vendorDir . '/google/cloud/CommonProtos/src'),
    'Google\\Auth\\' => array($vendorDir . '/google/auth/src'),
    'Google\\ApiCore\\' => array($vendorDir . '/google/gax/src'),
    'Google\\' => array($vendorDir . '/google/common-protos/src'),
    'GPBMetadata\\Google\\Spanner\\' => array($vendorDir . '/google/cloud/Spanner/metadata'),
    'GPBMetadata\\Google\\Pubsub\\' => array($vendorDir . '/google/cloud/PubSub/metadata'),
    'GPBMetadata\\Google\\Protobuf\\' => array($vendorDir . '/google/protobuf/src/GPBMetadata/Google/Protobuf'),
    'GPBMetadata\\Google\\Privacy\\Dlp\\' => array($vendorDir . '/google/cloud/Dlp/metadata'),
    'GPBMetadata\\Google\\Monitoring\\' => array($vendorDir . '/google/cloud/Monitoring/metadata'),
    'GPBMetadata\\Google\\Logging\\' => array($vendorDir . '/google/cloud/Logging/metadata'),
    'GPBMetadata\\Google\\Firestore\\' => array($vendorDir . '/google/cloud/Firestore/metadata'),
    'GPBMetadata\\Google\\Devtools\\Cloudtrace\\' => array($vendorDir . '/google/cloud/Trace/metadata'),
    'GPBMetadata\\Google\\Devtools\\Clouderrorreporting\\' => array($vendorDir . '/google/cloud/ErrorReporting/metadata'),
    'GPBMetadata\\Google\\Devtools\\Clouddebugger\\' => array($vendorDir . '/google/cloud/Debugger/metadata'),
    'GPBMetadata\\Google\\Datastore\\' => array($vendorDir . '/google/cloud/Datastore/metadata'),
    'GPBMetadata\\Google\\Container\\' => array($vendorDir . '/google/cloud/Container/metadata'),
    'GPBMetadata\\Google\\Cloud\\Websecurityscanner\\' => array($vendorDir . '/google/cloud/WebSecurityScanner/metadata'),
    'GPBMetadata\\Google\\Cloud\\Webrisk\\' => array($vendorDir . '/google/cloud/WebRisk/metadata'),
    'GPBMetadata\\Google\\Cloud\\Vision\\' => array($vendorDir . '/google/cloud/Vision/metadata'),
    'GPBMetadata\\Google\\Cloud\\Videointelligence\\' => array($vendorDir . '/google/cloud/VideoIntelligence/metadata'),
    'GPBMetadata\\Google\\Cloud\\Texttospeech\\' => array($vendorDir . '/google/cloud/TextToSpeech/metadata'),
    'GPBMetadata\\Google\\Cloud\\Tasks\\' => array($vendorDir . '/google/cloud/Tasks/metadata'),
    'GPBMetadata\\Google\\Cloud\\Talent\\' => array($vendorDir . '/google/cloud/Talent/metadata'),
    'GPBMetadata\\Google\\Cloud\\Speech\\' => array($vendorDir . '/google/cloud/Speech/metadata'),
    'GPBMetadata\\Google\\Cloud\\Securitycenter\\' => array($vendorDir . '/google/cloud/SecurityCenter/metadata'),
    'GPBMetadata\\Google\\Cloud\\Scheduler\\' => array($vendorDir . '/google/cloud/Scheduler/metadata'),
    'GPBMetadata\\Google\\Cloud\\Redis\\' => array($vendorDir . '/google/cloud/Redis/metadata'),
    'GPBMetadata\\Google\\Cloud\\Oslogin\\' => array($vendorDir . '/google/cloud/OsLogin/metadata'),
    'GPBMetadata\\Google\\Cloud\\Language\\' => array($vendorDir . '/google/cloud/Language/metadata'),
    'GPBMetadata\\Google\\Cloud\\Kms\\' => array($vendorDir . '/google/cloud/Kms/metadata'),
    'GPBMetadata\\Google\\Cloud\\Iot\\' => array($vendorDir . '/google/cloud/Iot/metadata'),
    'GPBMetadata\\Google\\Cloud\\Dialogflow\\' => array($vendorDir . '/google/cloud/Dialogflow/metadata'),
    'GPBMetadata\\Google\\Cloud\\Dataproc\\' => array($vendorDir . '/google/cloud/Dataproc/metadata'),
    'GPBMetadata\\Google\\Cloud\\Bigquery\\Datatransfer\\' => array($vendorDir . '/google/cloud/BigQueryDataTransfer/metadata'),
    'GPBMetadata\\Google\\Cloud\\Automl\\' => array($vendorDir . '/google/cloud/AutoMl/metadata'),
    'GPBMetadata\\Google\\Cloud\\Asset\\' => array($vendorDir . '/google/cloud/Asset/metadata'),
    'GPBMetadata\\Google\\Bigtable\\' => array($vendorDir . '/google/cloud/Bigtable/metadata'),
    'GPBMetadata\\Google\\' => array($vendorDir . '/google/cloud/CommonProtos/metadata', $vendorDir . '/google/common-protos/metadata', $vendorDir . '/google/gax/metadata'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/EmailValidator'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Composer\\Spdx\\' => array($vendorDir . '/composer/spdx-licenses/src'),
    'Composer\\Semver\\' => array($vendorDir . '/composer/semver/src'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'Composer\\' => array($vendorDir . '/composer/composer/src/Composer'),
    'ColorThief\\' => array($vendorDir . '/ksubileau/color-thief-php/lib/ColorThief'),
    'Aws\\' => array($vendorDir . '/aws/aws-sdk-php/src'),
    '' => array($vendorDir . '/google/grpc-gcp/src/generated'),
);
